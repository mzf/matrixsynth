matrixsynth
===========

A javascript matrix synthesizer using HTML5 audio tag.

Check the [demo page](http://moleculext.github.com/matrixsynth/).